package br.com.tmg.projeto.DAO;

import br.com.tmg.projeto.model.Item;
import br.com.tmg.projeto.model.Livro;
import java.util.List;
import javax.persistence.Query;

public class LivroDAO extends DAO<Livro> {

    public LivroDAO() {
        super(Livro.class);
    }

    public List<Livro> findByFiltro(String id, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Livro> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Livro a where 1=1 ");

        if (id != null && !id.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.titulo like :Titulo ");
        }

        Query query = em.createQuery(sql.toString());

        if (id != null && !id.isEmpty()) {
            query.setParameter("Id", new Long(id));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Titulo", "%" + nome + "%");
        }

        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }
    
    
    public static void main(String[] args) {

        LivroDAO dao = new LivroDAO();
        Livro livro12 = new Livro();
        livro12.setTitulo("uuuuuuuuuuuuuuu");
        livro12.setPaginas(888);
        livro12.setAutor("gffgfgfghf");
        livro12.setPrecoAntigo(22.50);
        livro12.setPrecoNovo(56.30);
        livro12.setImagem("oi");

        dao.save(livro12);

    }
    
}
