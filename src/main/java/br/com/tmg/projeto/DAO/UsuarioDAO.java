package br.com.tmg.projeto.DAO;

import br.com.tmg.projeto.model.Usuario;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class UsuarioDAO extends DAO<Usuario> {

    public UsuarioDAO() {
        super(Usuario.class);
    }

    public List<Usuario> findByFiltro(String id, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Usuario> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Usuario a where 1=1 ");

        if (id != null && !id.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }

        Query query = em.createQuery(sql.toString());

        if (id != null && !id.isEmpty()) {
            query.setParameter("Id", new Long(id));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }

        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }

    public Usuario findByUsername(String userName) {

        Usuario usuario = new Usuario();
        try {
            this.em = JPAUtil.getEntityManager();

            Query query = this.em.createQuery("from Usuario u where u.nome = :userName ");
            query.setParameter("userName", userName);
            usuario = (Usuario) query.getSingleResult();

            this.em.close();
        } catch (NoResultException ex) {
            ex.printStackTrace();
            return usuario;
        }

        return usuario;

    }

}
