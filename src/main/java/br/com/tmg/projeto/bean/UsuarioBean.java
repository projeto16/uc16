package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.UsuarioDAO;
import br.com.tmg.projeto.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.validation.ConstraintViolationException;

@Named(value = "usuarioBean")
@ViewScoped

public class UsuarioBean extends Bean implements Serializable {

    private Usuario usuario;
    private UsuarioDAO dao;

    private Usuario usuarioSelecionado;

    private List<Usuario> listar;

    private String id;
    private String nome;

    @PostConstruct
    public void init() {
        try {
            dao = new UsuarioDAO();
            usuarioSelecionado = new Usuario();
            listar = dao.findAll();

        } catch (Exception ex) {
            ex.printStackTrace();
            this.addMessageErro("Falha ao carregar itens.");
        }
    }

    public UsuarioBean() {
        this.usuario = new Usuario();
        this.dao = new UsuarioDAO();
    }

    public void salvar() {

        if (this.usuario.getId() == 0) {
            dao.save(usuario);
            this.addMessageInfo("Salvo com Sucesso!");
        } else {
            dao.update(usuario);
            this.addMessageInfo("Atualizado com sucesso!");
        }
    }

    public void pesquisa() {

        try {
            this.listar = this.dao.findByFiltro(id, nome);

        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }

    public void deletar(Usuario usuario) {

        try {

            this.dao.delete(usuario);
            this.addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                this.addMessageErro("Não é possível remover alunos cadastrados em cursos.");
            } else {
                this.addMessageErro("Falha ao remover aluno.");
                ex.printStackTrace();
            }

        }

    }
    
    
    public void novo(){
        this.usuario = new Usuario();
    }

    public Usuario getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /*
    public static void main(String[] args) {
       
        UsuarioDAO dao = new UsuarioDAO();
        Usuario u = new Usuario();
        u.setCelular("4454545454");
        u.setNome("jgjgggg");
        u.setCidade("hghg");
        u.setCpf("54545454");
        u.setEstado("es");
        u.setNascimento("123648");
        u.setEmail("nbjvvvvh");
        u.setEndereco("Rua A");
        u.setNumero(54);
        u.setSenha("15469345");
        
        dao.save(u);      

    }
     */
    public List<Usuario> getListar() {
        return listar;
    }

    public void setListar(List<Usuario> listar) {
        this.listar = listar;
    }

}
