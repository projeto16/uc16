/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.LivroDAO;
import br.com.tmg.projeto.model.Livro;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


@Named(value = "pesquisarLivroBean")
@ViewScoped
public class PesquisarLivroBean extends Bean{
    
     Livro livroSelecionado ;
     LivroDAO dao;
     private List<Livro> lista;
     
     String id;
     String nome;
    
    public PesquisarLivroBean() {
    }

    @PostConstruct
    void init(){
         try {
            this.livroSelecionado = new Livro();
            this.dao = new LivroDAO();
            this.lista = dao.findAll();
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
    
    public List<Livro> getLista() {
        return lista;
    }

    
}
