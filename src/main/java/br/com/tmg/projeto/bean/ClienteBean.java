package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.ClienteDAO;
import br.com.tmg.projeto.model.Cliente;
import java.io.Serializable;
import javax.faces.view.ViewScoped;

import javax.inject.Named;

@Named(value = "clienteBean")
@ViewScoped
public class ClienteBean extends Bean implements Serializable {

    private Cliente clienteSelecionado;

    private Cliente cliente;
    private ClienteDAO dao;

    private String nome;
    private String id;

    public ClienteBean() {
        this.cliente = new Cliente();
        this.dao = new ClienteDAO();
    }
    
    
    
    
    

    public void salvar() {

        if (this.cliente.getId() == 0) {
            dao.save(cliente);
            this.addMessageInfo("Cliente salvo com sucesso!");

        } else {
            dao.update(cliente);
            this.addMessageInfo("Usuario alterado com sucesso!");
        }

    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /*
     public static void main(String[] args) {
        
         Cliente c = new Cliente();
         ClienteDAO dao = new ClienteDAO();
         
         c.setNome("Glauberth");         
         c.setNascimento("00000000");
         c.setEndereco("Rua B");
         c.setEmail("c@gmail.com.br");
         c.setSenha("12345678");
         c.setCpf("1245987451");
         dao.save(c);
         
         
    }
     */
    

    public Cliente getClienteSelecionado() {
        return clienteSelecionado;
    }

    public void setClienteSelecionado(Cliente clienteSelecionado) {
        this.clienteSelecionado = clienteSelecionado;
    }

    public ClienteDAO getDao() {
        return dao;
    }

    public void setDao(ClienteDAO dao) {
        this.dao = dao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
