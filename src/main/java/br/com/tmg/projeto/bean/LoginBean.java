package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.UsuarioDAO;
import br.com.tmg.projeto.model.Usuario;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value = "loginBean")
@SessionScoped
public class LoginBean extends Bean  {

    private Usuario usuario;
    private Usuario usuarioLogado;
    private UsuarioDAO dao;

    public LoginBean() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    
    public String toHome(){
        
        dao = new UsuarioDAO() ; 
        
        usuarioLogado = dao.findByUsername(usuario.getNome()) ; 
        
        if(this.usuario.getSenha().equals(usuarioLogado.getSenha()) ){
            
            return "Home";
        }
        
        return null;
    }
     
     
}
