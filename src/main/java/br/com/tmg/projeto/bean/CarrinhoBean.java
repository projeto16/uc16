
package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.LivroDAO;
import br.com.tmg.projeto.model.Item;
import br.com.tmg.projeto.model.Livro;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "carrinhoBean")
@ViewScoped
public class CarrinhoBean implements Serializable{
    
      Livro livroSelecionado ;
      LivroDAO dao;
     private List<Item> lista;
     
     String id;
     String nome;
    
    public CarrinhoBean() {
    }

    @PostConstruct
    void init(){
         try {
            this.livroSelecionado = new Livro();
            this.dao = new LivroDAO();
           // this.lista = dao.findAll();
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
    
    public List<Item> getLista() {
        return lista;
    }

    
}
