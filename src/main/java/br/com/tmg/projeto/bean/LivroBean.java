package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.LivroDAO;
import br.com.tmg.projeto.model.Livro;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named(value = "livroBean")
@ViewScoped
public class LivroBean extends Bean implements Serializable {

    private Livro livro;
    private LivroDAO dao;

    public LivroBean() {
        this.livro = new Livro();
        this.dao = new LivroDAO();
    }

    public void salvar() {
        if (this.livro.getId() == 0) {
            dao.save(livro);
            this.addMessageInfo("Livro CADASTRADO com Sucesso!");
        } else {
            dao.update(livro);
            this.addMessageInfo("Livro ATUALIZADO com Sucesso!");
        }

    }

    /*
    public void adicionarCarrinho(int quantidade, double preco){
        
        Carrinho carrinho = new Carrinho();
        ArrayList<Item> lista = null ; 
        
        Item item = new Item();
        item.setLivro(livro);
        item.setPreco(preco);
        item.setQuantidade(quantidade);
        
        lista.add(item);
        carrinho.setLista(lista);
       
        
    }
     */
    public Livro getLivro() {
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }

}
